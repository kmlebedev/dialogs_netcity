import asyncio
import datetime
import hashlib
import json
import logging
import os
from collections import OrderedDict

import aiohttp

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class netcity():

    def __init__(self):
        self.login = os.environ.get('NETCITY_LOGIN')
        self.password = os.environ.get('NETCITY_PASSWORD')
        self.headers = {'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Referer': 'http://netcity.eimc.ru/?AL=Y',
        }
        self.api_url = 'http://netcity.eimc.ru/webapi'
        self.year_id = os.environ.get('NETCITY_YEAR_ID') or 175
        self.diary_url_format = '{api_url}/student/diary?studentId={studentId}&weekEnd={weekEnd}&weekStart={weekStart}&withLaAssigns=true&withPastMandatory=true&yearId={yearId}'
        self.studentdiary = {}
        self.loop = asyncio.get_event_loop()
        self.session = aiohttp.ClientSession(
            loop=self.loop,
            headers=self.headers,
        )

    async def _fetch(self, url):
        async with self.session.get(url, headers=self.headers) as response:
            return await response.text()


    async def _fetch_post(self, url, payload):
        async with self.session.post(url, data=payload) as response:
            return await response.json()


    def _get_auth_pw2(self, salt):
        return hashlib.md5(
            str(salt + hashlib.md5(self.password.encode('utf-8')).hexdigest()).encode('utf-8')
        ).hexdigest()


    async def _auth(self):
        auth_getdata = await self._fetch_post('{}/auth/getdata'.format(self.api_url), {})
        auth_pw2 = self._get_auth_pw2(auth_getdata.get('salt'))
        auth_login_post_data = {
            'LoginType': '1',
            'cid': '2',
            'sid': '66',
            'pid': '-1',
            'cn': '3',
            'sft': '2',
            'scid': '23',
            'UN': self.login,
            'PW': auth_pw2[:len(self.password)],
            'lt': auth_getdata.get('lt'),
            'pw2': auth_pw2,
            'ver': auth_getdata.get('ver')
        }
        auth_login = await self._fetch_post('{}/login'.format(self.api_url), auth_login_post_data)
        self.headers.update({'at': auth_login['at']})


    def _get_lessons_assignmens(self, student_id):
        if student_id not in self.studentdiary:
            yield

        for week_day, lessons in self.studentdiary[student_id].items():
            for lesson in lessons:
                if 'subjectName' not in lesson or 'assignments' not in lesson:
                    continue
                yield '{}: {}'.format(
                    lesson['subjectName'],
                    ', '.join([assignment['assignmentName'] for assignment in lesson['assignments']])
                )

    def _get_last_lessons_assignmens(self, student_id):
        if student_id not in self.studentdiary:
            yield

        lessons = list(self.studentdiary[student_id].values())[-1]
        for lesson in lessons:
            if 'subjectName' not in lesson or 'assignments' not in lesson:
                continue
            yield '{}: {}'.format(
                lesson['subjectName'],
                ', '.join([assignment['assignmentName'] for assignment in lesson['assignments']])
            )


    async def _update_assignment_week(self, student_id):
        today = datetime.date.today()
        start_of_week = today - datetime.timedelta(days=today.weekday(), weeks=1)
        await self.__update_assignment(student_id, str(start_of_week), str(today))

    async def _update_assignment_today(self, student_id):
        today = str(datetime.date.today())
        await self.__update_assignment(student_id, today, today)

    async def __update_assignment(self, student_id, week_start, week_end):
        studentdiary_url = self.diary_url_format.format(
            api_url=self.api_url,
            studentId=student_id,
            weekStart=week_start,
            weekEnd=week_end,
            yearId=self.year_id
        )

        if 'at' not in self.headers:
            await self._auth()

        html = await self._fetch(studentdiary_url)
        if html == 'Ошибка доступа':
            logger.error('access error')
            logger.error(html)
            await self._auth()
            html = await self._fetch(studentdiary_url)

        diary = json.loads(html)
        for week_day in diary['weekDays']:
            if student_id not in self.studentdiary:
                self.studentdiary[student_id] = OrderedDict()
            if week_day['date'] in self.studentdiary[student_id]:
                self.studentdiary[student_id][week_day['date']].update(week_day['lessons'])
            else:
                self.studentdiary[student_id][week_day['date']] = week_day['lessons']


    def get_assignments_today(self, student_id):
        self.loop.run_until_complete(self._update_assignment_week(student_id))
        return list(self._get_last_lessons_assignmens(student_id))


    async def _get_user_info(self, token):
        '''
        https://yandex.ru/dev/passport/doc/dg/reference/response-docpage/
        {
          "login": "vasya",
          "old_social_login": "uid-mmzxrnry",
          "default_email": "test@yandex.ru",
          "id": "1000034426",
          "client_id": "4760187d81bc4b7799476b42b5103713",
          "emails": [
            "test@yandex.ru",
            "other-test@yandex.ru"
          ],
          "openid_identities": [
            "http://openid.yandex.ru/vasya/",
            "http://vasya.ya.ru/"
          ]
        }
        '''
        url = 'https://login.yandex.ru/info'
        headers = {'Authorization': 'OAuth {}'.format(token)}
        async with aiohttp.ClientSession(loop=self.loop) as session:
            async with session.get(url, headers=headers) as resp:
                return await resp.json()

    async def update_user_email(self, token, user_id):
        user_info = await self._get_user_info(token)
        user_to_email[user_id] = user_info.get('default_email')

netcity = netcity()
user_to_student_id = {}
# Todo on initial get table from netcity
email_to_student_id = {'whitef0x@yandex.ru': 64413}
user_to_email = {}

def handler(event, context):
    """
    https://yandex.ru/dev/dialogs/alice/doc/protocol-docpage/#create
    {
      "meta": {
        "locale": "ru-RU",
        "timezone": "Europe/Moscow",
        "client_id": "ru.yandex.searchplugin/5.80 (Samsung Galaxy; Android 4.4)",
        "interfaces": {
          "screen": { }
        }
      },
      "request": {
        "command": "закажи пиццу на улицу льва толстого 16 на завтра",
        "original_utterance": "закажи пиццу на улицу льва толстого, 16 на завтра",
        "type": "SimpleUtterance",
        "markup": {
          "dangerous_context": true
        },
        "payload": {},
      },
      "session": {
        "new": true,
        "message_id": 4,
        "session_id": "2eac4854-fce721f3-b845abba-20d60",
        "skill_id": "3ad36498-f5rd-4079-a14b-788652932056",
        "user_id": "AC9WC3DF6FCE052E45A4566A48E6B7193774B84814CE49A922E163B8B29881DC"
      },
      "version": "1.0"
    }
    Authorization: Bearer <access_token>
    """
    logger.info('## EVENT')
    logger.info(event)

    response = {'version': event.get('version'), 'session': event.get('session')}
    authorization = event.get('headers', {}).get('Authorization', {})

    if 'account_linking' not in event.get('meta', {}).get('interfaces', {}):
        response['response'] = {'text': 'Послан', 'end_session': 'true'}
    elif not authorization:
        response['start_account_linking'] = {}
    else:
        try:
            user_id = event['session']['user_id']
            if user_id not in user_to_student_id:
                netcity.loop.run_until_complete(netcity.update_user_email(authorization[len('Bearer '):], user_id))
                if user_to_email[user_id] not in email_to_student_id:
                    text = 'Я не нашла твой e-mail в персональных настройках, ' \
                           'попробуй указать {}'.format(user_to_email[user_id])
                    response['response'] = {'text': text, 'end_session': 'true'}
                    return response
                user_to_student_id[user_id] = email_to_student_id[user_to_email[user_id]]
            assignments = netcity.get_assignments_today(user_to_student_id[user_id])
            if assignments:
                text = ';'.join(assignments)
            else:
                text = 'Заданий нет'
            response['response'] = {'text': text, 'end_session': 'false'}
        except:
            response['response'] = {'text': 'Что то пошло не так', 'end_session': 'true'}

    return response
